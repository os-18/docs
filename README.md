<!-- ![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg) -->

- Documentation: [os-18.gitlab.io/docs/amalthea.html](https://os-18.gitlab.io/docs/amalthea.html)

- Main repository: [gitlab.com/os-18/amalthea](https://gitlab.com/os-18/amalthea)
